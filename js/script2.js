function checkIsAgreed(){
    if($('#i_agree input[type="checkbox"]').prop('checked'))
        $("#submit_btn").prop('disabled', false);
    else
        $("#submit_btn").prop('disabled', true);
}
 $(document).ready(function () {
    checkIsAgreed();
    /*$('#city').on("input", function(e){
        getCity($(this).val());
    });*/
    /*$('#city').on("input", function(e){
        let t = $(this).val().toLowerCase();
        let x = cities.filter(function(city){
            return (city.toLowerCase() == t);
        });
        console.log();
    });*/
    $('#i_agree input[type="checkbox"]').on('change', function(){
        checkIsAgreed();
    });
    $('#fincity_logo').click(function(){
        $('#fincity_logo').addClass('img-focus-in');
        setTimeout(function() {
            $('#fincity_logo').removeClass('img-focus-in');
        }, 500);
    });

    var itemsMainDiv = ('.MultiCarousel');
    var itemsDiv = ('.MultiCarousel-inner');
    var itemWidth = "";

    $('.leftLst, .rightLst').click(function () {
        var condition = $(this).hasClass("leftLst");
        if (condition)
            click(0, this);
        else
            click(1, this)
    });

    ResCarouselSize();

    $(window).resize(function () {
        ResCarouselSize();
    });

    //this function define the size of the items
    function ResCarouselSize() {
        var incno = 0;
        var dataItems = ("data-items");
        var itemClass = ('.item');
        var id = 0;
        var btnParentSb = '';
        var itemsSplit = '';
        var sampwidth = $(itemsMainDiv).width();
        var bodyWidth = $('body').width();
        $(itemsDiv).each(function () {
            id = id + 1;
            var itemNumbers = $(this).find(itemClass).length;
            btnParentSb = $(this).parent().attr(dataItems);
            itemsSplit = btnParentSb.split(',');
            $(this).parent().attr("id", "MultiCarousel" + id);
            if (bodyWidth >= 1200) {
                incno = itemsSplit[3];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 992) {
                incno = itemsSplit[2];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 768) {
                incno = itemsSplit[1];
                itemWidth = sampwidth / incno;
            }
            else {
                incno = itemsSplit[0];
                itemWidth = sampwidth / incno;
            }
            $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
            $(this).find(itemClass).each(function () {
                $(this).outerWidth(itemWidth);
            });

            $(".leftLst").addClass("over");
            $(".rightLst").removeClass("over");

        });

        /*Change Password start*/
         var specialPattern = new RegExp("[^A-Za-z0-9]");
            var letterPattern = new RegExp("([^\0]*[a-zA-Z]{1,}[^\0]*){8,16}");
            var numberPattern = new RegExp("(?=.*[0-9])+");

            $('input[type=checkbox]').on('change', function(){
                    if($('input[type=checkbox]').prop('checked'))
                    {
                        $('.btn.btn-info.btn-lg.disabled').removeClass('disabled');
                    }else{
                        $('.btn.btn-info.btn-lg').addClass('disabled');
                    }
                });


            $('#pwd').on('input', function(){
                var stringVal = $('#pwd').val();
                if(stringVal == undefined || stringVal == "" || stringVal == null )
                {
                 return;
                }
                if(letterPattern.test(stringVal))
                    $('#test1').addClass('show-tick');
                else
                    $('#test1').removeClass('show-tick');
                if(specialPattern.test(stringVal))
                    $('#test2').addClass('show-tick');
                else
                    $('#test2').removeClass('show-tick');
                if(numberPattern.test(stringVal))
                    $('#test3').addClass('show-tick');
                else
                    $('#test3').removeClass('show-tick');

            });

            var cookieWarned = window.localStorage.getItem('cookie-warning-shown');
            if (cookieWarned != "yes") {
                $('.cookie-alert').removeClass('hidden');
            }

            $('.cookie-alert .action button').click(function() {
                window.localStorage.setItem('cookie-warning-shown', 'yes');
                $('.cookie-alert').addClass('hidden');
            });

        /*change Password End*/
    }
    //this function used to move the items
    function ResCarousel(e, el, s) {
        var leftBtn = ('.leftLst');
        var rightBtn = ('.rightLst');
        var translateXval = '';
        var divStyle = $(el + ' ' + itemsDiv).css('transform');
        var values = divStyle.match(/-?[\d\.]+/g);
        var xds = Math.abs(values[4]);
        if (e == 0) {
            translateXval = parseInt(xds) - parseInt(itemWidth * s);
            $(el + ' ' + rightBtn).removeClass("over");

            if (translateXval <= itemWidth / 2) {
                translateXval = 0;
                $(el + ' ' + leftBtn).addClass("over");
            }
        }
        else if (e == 1) {
            var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
            translateXval = parseInt(xds) + parseInt(itemWidth * s);
            $(el + ' ' + leftBtn).removeClass("over");

            if (translateXval >= itemsCondition - itemWidth / 2) {
                translateXval = itemsCondition;
                $(el + ' ' + rightBtn).addClass("over");
            }
        }
        $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
    }

    //It is used to get some elements from btn
    function click(ell, ee) {
        var Parent = "#" + $(ee).parent().attr("id");
        var slide = $(Parent).attr("data-slide");
        ResCarousel(ell, Parent, slide);
    }

    // Add smooth scrolling to all links
    $("a:not(.no-scroll-top)").on('click', function(event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: ($(hash).offset().top - 75)
            }, 800, function() {});
        }   
    });

    $('.scroll-top').fadeOut(100);
    var $win = $(window);
    var winH = $win.height(); // Get the window height.
    $win.on("scroll", function() {
        if ($(this).scrollTop() > (winH - 75)) {
            $('.scroll-top').fadeIn(100);
        } else {
            $('.scroll-top').fadeOut(100);
        }
    }).on("resize", function() { // If the user resizes the window
        winH = $(this).height(); // you'll need the new height value
    });

});

function seeAll(slide_up_var){
    if(slide_up_var){
        $('#all').css('opacity',0);
        $('#less_show').css('opacity',1);
        $('#slide-up').removeClass('slide-up');
        $('#slide-up').addClass('slide-down');
        $('#less_show').css('bottom',0);
    }
    else{
        $('#less_show').css('bottom','-19px');
        $('#less_show').css('opacity',0);
        $('#all').css('opacity',1);
        $('#slide-up').removeClass('slide-down');
        $('#slide-up').addClass('slide-up');
    }
}
seeAll(false);

function hideShowPassword(ids){
    var x = document.getElementById(ids);
    if(x.type === "password"){
        x.type = "text";
        $('#' + ids +' + button img:nth-child(1)').removeClass('hide');
        $('#' + ids +' + button img:nth-child(1)').addClass('show');
        $('#' + ids +' + button img:nth-child(2)').removeClass('show');
        $('#' + ids +' + button img:nth-child(2)').addClass('hide');
    }
    else{
        x.type = "password";
        $('#' + ids +' + button img:nth-child(1)').removeClass('show');
        $('#' + ids +' + button img:nth-child(1)').addClass('hide');
        $('#' + ids +' + button img:nth-child(2)').removeClass('hide');
        $('#' + ids +' + button img:nth-child(2)').addClass('show');
    }
}

var cities = ['New Delhi','Mumbai','Bengaluru','Hyderabad','Chennai','Kolkata','Ahmedabad','Gurugram','Pune','Ahmedabad'];
function getCurrentLocation() {
       if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            console.log("Geolocation is not supported by this browser.");
        }
}
function showPosition(position) {
    $.get("https://maps.googleapis.com/maps/api/geocode/json?sensor=false&language=en&latlng=" + position.coords.latitude + "," + position.coords.longitude + "&key=AIzaSyBSf6TIdNnJPns4fe5Br-JTUHCdR22v46w", function(res, status) {
        if (status == "success" && res.status == "OK") {
            var data = res;
            if (data && data.results && data.results.length > 0) {
                var location_city = "";
                var location_state = "";
                for (var i = 0; i < data.results.length; i++) {
                    for (var j = 0; j < data.results[i].address_components.length; j++) {
                        if (data.results[i].address_components[j].types.indexOf('locality') > -1) {
                            location_city = data.results[i].address_components[j].short_name;
                            break;
                        }
                        if (data.results[i].address_components[j].types.indexOf('administrative_area_level_1') > -1) {
                            location_state = (data.results[i].address_components[j].short_name);
                            break;
                        }
                    }
                }
                showCityState(location_city,location_state);
            }
        }
    });
}
function getCity(city) {
    $.get("https://maps.googleapis.com/maps/api/geocode/json?sensor=false&language=en&address=" + city +"&key=AIzaSyBSf6TIdNnJPns4fe5Br-JTUHCdR22v46w", function(res, status) {
        if (status == "success" && res.status == "OK") {
            var  data = res;
            var searchBox = new google.maps.places.SearchBox(city);
            console.log(searchBox);
            console.log(data);
            if (data && data.results && data.results.length > 0) {
                var location_city = "";
                var location_state = "";
                for (var i = 0; i < data.results.length; i++) {
                    for (var j = 0; j < data.results[i].address_components.length; j++) {
                        if (data.results[i].address_components[j].types.indexOf('locality') > -1) {
                            location_city = data.results[i].address_components[j].short_name;
                            break;
                        }
                        if (data.results[i].address_components[j].types.indexOf('administrative_area_level_1') > -1) {
                            location_state = (data.results[i].address_components[j].short_name);
                            break;
                        }
                    }
                }
                //showCityState(location_city,location_state);
            }
        }
    });
}
function showCityState(location_city, location_state){
    $('#location').text('');
    $('#location').text(location_city + ", " + location_state);
    $('#city').val('');
    $('#city').val(location_city);
    $.each(cities, function( index, value ) {
        if(location_city == value){
            $(".row.city-list-section .col-md-2.city-list-items.active").removeClass('active');
            $(".row.city-list-section2 .col-md-4.city-list-items.active").removeClass('active');
            var pos = index + 1;
            if(pos > 6){
                $(".row.city-list-section2 .col-md-4.city-list-items:nth-child" + (pos - 6) + ")").addClass('active');
            }
            else{
                $(".row.city-list-section .col-md-2.city-list-items:nth-child(" + pos +")" ).addClass('active');
            }
        }
    });
}
getCurrentLocation();      

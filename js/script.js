 $(document).ready(function() {
    	var specialPattern = new RegExp("[^A-Za-z0-9]");
        var letterPattern = new RegExp("([^\0]*[a-zA-Z]{1,}[^\0]*){8,16}");
        var numberPattern = new RegExp("(?=.*[0-9])+");

        var cookieWarned = window.localStorage.getItem('cookie-warning-shown');
        if (cookieWarned != "yes") {
            $('.cookie-alert').removeClass('hidden');
        }

        $('.cookie-alert .action button').click(function() {
            window.localStorage.setItem('cookie-warning-shown', 'yes');
            $('.cookie-alert').addClass('hidden');
        });
     	
        _init_fn();
        
        $('#accept-terms').on('change', function(){
            if($(this).prop('checked'))
            {
                $('.btn.btn-info.btn-lg.disabled').removeClass('disabled');
            }else{
                $('.btn.btn-info.btn-lg').addClass('disabled');
            }
        });

        $('#fincity_logo').click(function(){
	        $(this).addClass('img-focus-in');
	        setTimeout(function() {
	            $('#fincity_logo').removeClass('img-focus-in');
	        }, 500);
	    });
		$("#signup").validate({
            debug:true,
			 onfocusout: function(element) {
	            this.element(element);
	        },
	        submitHandler: function (form) {
	            return false;
	        }
		});
        $("#otpForm").validate({
            debug:true,
            rules:{
                enterOtp:{
                    required:true,
                    minlength:4,
                    maxlength:4,
                }
            },
            messages:{
                enterOtp:{
                    required: "Please enter OTP sent to your mobile number",
                    minlength: "Please enter OTP sent to your mobile number",
                    maxlength: "Please enter OTP sent to your mobile number",
                }
            },
             onfocusout: function(element) {
                this.element(element);
            },
            submitHandler: function (form) {
                return false;
            }
        });
		/*$("#loginForm").validate({
			onfocusout: function(element) {
	            this.element(element);
	        },
	        submitHandler: function (form) {
	            return false;
	        }
		});
        */
        $("#registerMobile").rules("add",
        {   regx: /^([7-9]){1}(\d)(?!\1+$)\d{8}?$/,
            minlength: 10,
            "messages":{
                minlength: "Please enter valid mobile number",
            },
        });
        $("#registerConfirmPassword").rules("add",
        {   checkPwd: "#registerPassword"
        });
        
        $("#registerPassword").rules("add",
        {   password_pattern: [specialPattern,letterPattern,numberPattern]
        }); 

        $.validator.addMethod("regx", function(value, element, regexpr) {          
            return regexpr.test(value);
        }, "Please enter valid mobile number");

        $.validator.addMethod("checkPwd", function(confirmPwd, element, pwd) {          
             if ($(pwd).val() == confirmPwd){
                return true;
             }
        }, "Passwords do not match");

         $.validator.addMethod("password_pattern", function(pass, element, pattern) {          
             if((pattern[0].test(pass)) && (pattern[1].test(pass)) && (pattern[2].test(pass)) )
                return true;
            }
        , "Please enter password as per format shown below");

         $("#forgotPassword").validate({
            onfocusout: function(element) {
                this.element(element);
            },
            submitHandler: function (form) {
                return false;
            }
        });
        $("#change_password").validate({
            rules:{
                cp_Password: {
                    password_pattern: [specialPattern,letterPattern,numberPattern],
                },
                cp_ConfirmPassword: {
                    checkPwd: "#cp_Password"
                },

            },
            onfocusout: function(element) {
                this.element(element);
            },
            submitHandler: function (form) {
                return false;
            }
        });
        
        $("input.check-input").blur(function(){
             if( $(this).val().length !== 0 ) {
            console.log($(this).val());
                $("#" + this.id + " ~ label.form-control-placeholder").addClass('not-empty');
            }
            else{
                $('#'+ this.id + ' ~ label.form-control-placeholder').removeClass('not-empty');                
            }
        });


        $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
                /*if ((event.which < 48 || event.which > 57)) {
                    if((event.which == 9 || event.which  == 37  || event.which == 38 || event.which  ==  39 || event.which  ==  40 ))*/
                    if ((event.which < 48 || event.which > 57)) 
                    {
                        event.preventDefault();
                    }
                
        });
        $('#registerPassword, #cp_Password').on('input', function(){
            var stringVal = $(this).val();
            if(stringVal == undefined || stringVal == "" || stringVal == null )
            {
            	$('#test1').css('display','none');
				$('#test2').css('display','none');
				$('#test3').css('display','none');
            	$('#test1 + img').css('display','inline-block');
				$('#test2 + img').css('display','inline-block');
				$('#test3 + img').css('display','inline-block');
             	return;
            }
            if(letterPattern.test(stringVal)){
                $('#test1').css('display','inline-block');
                $('#test1 + img').css('display','none');
            }
            else{
                $('#test1').css('display','none');
                $('#test1 + img').css('display','inline-block');
            }
            if(specialPattern.test(stringVal)){
                $('#test2').css('display','inline-block');
                $('#test2 + img').css('display','none');
            }
            else{
                $('#test2').css('display','none');
                $('#test2 + img').css('display','inline-block');
            }
            if(numberPattern.test(stringVal)){
                $('#test3').css('display','inline-block');
                $('#test3 + img').css('display','none');
            }
            else{
                $('#test3').css('display','none');
                $('#test3 + img').css('display','inline-block');
            }

        });
        $('.chat-icon').click(function(){
            $('.chat-window').toggleClass('hide');
        });

        var $nav = $('body');
        var $header = $('#navbar1');
        var $win = $(window);
        var winH = $win.height(); // Get the window height.
        $('.scroll-top').fadeOut(100);

        $win.on("scroll", function() {
            /*if (($win.width() >= 600)) {*/
                if($(this).scrollTop() > (winH - 45))
                    $('.scroll-top').fadeIn(100);
                else
                    $('.scroll-top').fadeOut(100);
        }).on("resize", function() { // If the user resizes the window
            winH = $(this).height(); // you'll need the new height value
        });

         $("a:not(#navbar3 a)").on('click', function(event) {
            if (this.hash !== "") {
                event.preventDefault();
                var hash = this.hash;
                $('html, body').animate({
                    scrollTop: ($(hash).offset().top - 45)
                }, 800, function() {});
            }
        });
        $("#navbar3 a").on('click', function(event) {
            if (this.hash !== "") {
                event.preventDefault();
                var hash = this.hash;
                $('html, body').animate({
                    scrollTop: ($(hash).offset().top - 128)
                }, 800, function() {});
            }
        }); 
        $(".scroll-top a").on('click', function(event) {
            var headerHeight;
            if($win.width() >= 600)  headerHeight = 45;  
            else  headerHeight = 125;  

            if (this.hash !== "") {
                event.preventDefault();
                var hash = this.hash;
                $('html, body').animate({
                    scrollTop: ($(hash).offset().top - headerHeight)
                }, 800, function() {});
            }
        });
});
function hideShowPassword(ids){
    var x = document.getElementById(ids);
    
	    if(x.type === "password"){
	        x.type = "text";
	        $('#' + ids +' ~ button > img:nth-child(1)').css('display','none');
	        $('#' + ids +' ~ button > img:nth-child(2)').css('display','inline-block');
	    }
	    else{
	        x.type = "password";
            $('#' + ids +' ~ button > img:nth-child(1)').css('display','inline-block');
            $('#' + ids +' ~ button > img:nth-child(2)').css('display','none');
        }
}
function _init_fn(){
    $("#accept-terms, #gridCheck1").prop('checked', false); 
    /*Preloader start*/
        if (window.showPreLoader) {        
            window.showPreLoader();
        }
    /*Preloader end*/
}

window.showPreLoader = function(){
          $("#depreload").css('display','table');
          setTimeout(function(){
              $("#depreload .wrapper").animate({ opacity: 1 });
          }, 400);

          setTimeout(function(){
              $("#depreload .logo").animate({ opacity: 1 });
          }, 800);

          var canvas  = $("#depreload .line")[0];
          var context = canvas.getContext("2d");

          context.beginPath();
          context.arc(100, 100, 99, Math.PI * 1.5, Math.PI * 1.6);
          context.strokeStyle = '#0077b5';
          context.lineWidth = 3;
          context.stroke();


          var percent = 0;
          window.setInterval(function(){
              if(percent < 100){
                  percent  = percent + 1;
                  $("#depreload .line").animate({ opacity: 1 });
                  context.clearRect(0, 0, canvas.width, canvas.height);
                  context.beginPath();
                  context.arc(100, 100, 99, Math.PI * 1.5, Math.PI * (1.5 + percent / 50), false);
                  context.stroke();
              }
              else{
                  /*percent  = 0;*/
                  $("#depreload .loading").animate({ opacity: 0 });
              }
          }, 1);
          setTimeout(function(){ $("#depreload").css('display','none'); }, 2000);
};
var type_alerts = ['Alert', 'Oops!', 'Success', 'Warning'];
var array = [];
var newHTML = [];
var index = 0;
function showAlerts(value){
        

        var alertMessage = "";
        var typeMessage = type_alerts[value.type];
        index += 1;
    
        alertMessage += '<div class="row messages" id="message_' + (index) + '" style="opacity:1"><div class="col-xs-2 no-padding-cols';
        
        if((value.type == 0)||( value.type == 1))
            alertMessage += ' bg-danger"><div class="messages-img-container"><div class="text-center messages-img">';
        else if(value.type == 2)
            alertMessage += ' bg-success"><div class="messages-img-container"><div class="text-center messages-img">';
        else if(value.type == 3)
            alertMessage += ' bg-warning"><div class="messages-img-container"><div class="text-center messages-img">';
        
        if((value.type == 0)||( value.type == 1)){
            alertMessage += '<img src="images/Oops.svg" width="20" />';
        }
        else if(value.type == 2){
            alertMessage += '<img src="images/Done.svg" width="20" />';
        }
        else if(value.type == 3){
            alertMessage += '<img src="images/Warning.svg" width="20" />';
        }
        alertMessage += '<div>' + typeMessage + '</div></div></div></div><div class="col-xs-10 hide-overflow" style="width: 83%;"><div class="message-text"><div>' + value.message + '</div><button type="button" onclick="removeAlert(' + (index) + ')" class="close close-abs-btn"><span class="icon icon-close"></span></button></div></div></div>';
        newHTML.push(alertMessage);
         
    $(".alertMessages").html(newHTML.join(""));
        setTimeout(function(){ 
            removeAlert(index);
        }, 5000);
    
}

function removeAlert(x){
    console.log(x);
    $('#message_' + x).css('opacity','0');
    setTimeout(function(){ 
        $('#message_' + x).css('display','none');
    }, 302);

}
function addAlert(x){
    var last_alert = (array[array.length - 1]);
    if (last_alert && (last_alert.message == x.message))
    return;

     array.push(x);
     showAlerts(x);
}
/*addAlert({id: 5, message: 'Alert message ', type: 1 });*/
